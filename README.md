# GitHub is my main platform: https://github.com/spydr06

```
~$ neofetch   
                    *.                          Spydr06@Germany   
                **********          ****        ----------------
            ****************************,       Uptime: 16 years
         ****%%%%%%%%%%%%%%%%%%%%%%%%%*****     Programming Languages: CSpydr, C, C++, Rust,
        *****%.      %  %%, /%%  %%%%%******                           Go, V, Java, x86 Assembly
        *****%.  %%%%%%  %   %  %%%%%%******    OS: Arch Linux, Fedora
       .*****%.     %%%    %    %%%%%%****      WM: bspwm, GNOME
       ******%.  %%%%%%%  (%*  %%%%%%%**        Shell: bash, zsh
       ******%%%%%%%%%%%%%%%%%%%%%%%%%          Editor: VSCodium, nano, Emacs
      *******%%%%%%%%%%%%%%%%%%%%%%%%%          Interests: Linux, Compilers, VMs, OSes, "Low-Level Stuff"
      *******%%%%%%%%%%%%%%%%%%%%%%%%%*****,    Top Project: CSpydr
     ,*******%%%%%%%%%%%%%%%%%%%%%%%%%*******   Dotfiles: https://github.com/spydr06/dotfiles.git
      *******%%________%%%%%%%%%%%%%%%******    
         ****%%%%%%%%%%%%%%%%%%%%%%%%%*****     Contacts  
             **********  ****************       --------
                ****        .***********        Reddit: u/mcspiderfe
                                      *.        Homepage: https://spydr06.github.io
```
